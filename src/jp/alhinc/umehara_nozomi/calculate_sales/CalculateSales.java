package jp.alhinc.umehara_nozomi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {
	public static void main(String[] args){
		//コマンドライン引数が1つでない場合のエラー処理
		if( args.length == 0 || args.length >=2 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		// 支店定義ファイルのマップの宣言
		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		// 支店コードに0を入れるマップ宣言
		HashMap<String, Long> salesMap = new HashMap<String, Long>();
		BufferedReader br = null;
		BufferedWriter bw = null;
		//読み込みメソッド呼び出し
		if(!listFileRead(args[0],"branch.lst",branchNameMap,salesMap)){
			return;
		}
		// フィルタを作成する
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str){
				File fileStr = new File(file,str);
				// 指定文字列でフィルタする
				if (str.matches("[0-9]{8}.rcd$") && fileStr.isFile()){
					return true;
				}else{
					return false;
				}
			}
		};
		// “rcd”が含まれるフィルタを作成する
		File[] files = new File(args[0]).listFiles(filter);
		// 連番チェック（1ずつ増加している）
		int beforeFileNum;
		for(int i=1; i<files.length; ++i){
			File salesFile = files[i];
			String file1;
			file1 = files[i-1].getName();
			String fileName = salesFile.getName();
			int FileNum = Integer.parseInt(fileName.replaceAll("[^0-9]",""));
			beforeFileNum = Integer.parseInt(file1.replaceAll("[^0-9]",""));
			if(FileNum - beforeFileNum !=1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int i=0; i<files.length; ++i){
			ArrayList<String> stringList = new ArrayList<String>();
			BufferedReader br2 = null;
			try{
				File salesFile = files[i];
				FileReader fr2 = new FileReader(salesFile);
				br2 = new BufferedReader(fr2);
				String line;
				while ((line = br2.readLine()) != null){
					// リストに追加
					stringList.add(line);
				}
				// 売上ファイル2桁ではない場合のエラー処理
				 if(stringList.size() !=2 ){
					 System.out.println(salesFile.getName() + "のフォーマットが不正です");
					 return;
				  }
				//売上ファイルが数字じゃない場合のエラー処理
				String check;
				check = stringList.get(1);
				if(!check.matches("[0-9]+")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				String branch = stringList.get(0);
				Long sales = salesMap.get(branch);
				// 支店コードがない場合のエラー処理
				if (!salesMap.containsKey(branch)){
					System.out.println(salesFile.getName() + "の支店コードが不正です");
					return;
				}
				// Long型に変換
				long ln = Long.parseLong(stringList.get(1));
				// 売上ファイルの金額の加算
				salesMap.put(branch,sales + ln);
				// 金額が10桁以上になった場合のエラー処理
				if(sales+ln >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally{
				if(br2 != null){
					try{
						br2.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		//出力メソッド呼び出し
		if(!salesOut(args[0],"branch.Out",branchNameMap,salesMap)){
			return;
		}
	}
	//ファイル読み込みメソッド
	public static boolean listFileRead(String fileplace,String fileName,HashMap<String,String> branchMap,HashMap<String,Long>salesMap){
		BufferedReader br = null;
		try{
			File file = new File(fileplace,fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null){
				// 文字列をカンマの前後で分割
				String[] lines = line.split(",");
				// 数字3桁かつ配列数が2ではない
				if(!lines[0].matches("[0-9]{3}") || lines.length !=2){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				// 支店コード,支店名
				branchMap.put(lines[0],lines[1]);
				// 支店コード,0
				salesMap.put(lines[0],0L);
			}
		}catch(IOException e){
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	//ファイル出力メソッド
	public static boolean salesOut(String filePath,String fileName,HashMap<String,String> nameMap,HashMap<String,Long> salesMap){
		BufferedWriter bw = null;
		try{
			File file = new File(filePath,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (HashMap.Entry<String, String> entry : nameMap.entrySet()) {
				// Mapのキーと値の両方をentrySetメソッドで取得
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey())+ "\n" );
			}
			return true;
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null){
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
}
