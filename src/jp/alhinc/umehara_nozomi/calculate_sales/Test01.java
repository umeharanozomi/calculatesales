
package jp.alhinc.umehara_nozomi.calculate_sales;
import java.io.File;
import java.io.FilenameFilter;

public class Test01 {
	public static void main(String[] args) {
        //フィルタを作成する
        FilenameFilter filter = new FilenameFilter(){
            public boolean accept(File file, String str){

                //指定文字列でフィルタする
                if(str.indexOf("rcd") != -1) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        //listFilesメソッドを使用して一覧を取得する
        File[] list = new File("C:\\Users\\umehara.nozomi\\Desktop\\売上集計課題").listFiles(filter);

        if(list != null) {

            for(int i=0; i<list.length; i++) {

                //ファイルの場合
                if(list[i].isFile()) {
                    System.out.println("ファイルです : " + list[i].toString());
                }
                //ディレクトリの場合
                else if(list[i].isDirectory()) {
                    System.out.println("ディレクトリです : " + list[i].toString());
                }
            }
        } else {
            System.out.println("null");
        }
    }

}
